PROJECT_NAME := "sconn"
PKG := "satnamram.org/golang/$(PROJECT_NAME)"
PKG_LIST := $(shell go list ${PKG}/... | grep -v /vendor/)
GO_FILES := $(shell find . -name '*.go' | grep -v /vendor/ | grep -v _test.go)

.PHONY: all dep clean test coverage coverhtml lint

all: test

lint:
	@golint -set_exit_status ${PKG_LIST}

test: dep
	@go test -short ${PKG_LIST}

race: dep
	@go test -race -short ${PKG_LIST}

msan: dep
	@CC=clang go test -msan -short ${PKG_LIST}

coverage: dep
	@coverage

coverhtml: dep
	@coverage html && mv coverage.html ${CI_PROJECT_DIR}/coverage.html;

dep:
	@go get -v -d ./...

clean:
	@rm -f $(PROJECT_NAME)

