package sconn

import (
	"crypto/x509/pkix"
	"strings"
	"testing"
	"time"
)

// TODO: table driven test for all encryption options
func TestKeygenECDSAChain(t *testing.T) {

	// generate ca cert and key
	caKG := NewKeygenECDSA(EcdsaCurveP521)
	caCert, caKey, err := caKG.Generate(ServerClientCA, nil, pkix.Name{CommonName: "TestName1"}, time.Now(), 1*time.Hour)
	if err != nil {
		t.Fatal(err)
	}

	// generate c1 cert and key
	c1KG := NewKeygenECDSA(EcdsaCurveP521)
	err = c1KG.SetCaCert(caCert, caKey)
	if err != nil {
		t.Fatal(err)
	}
	c1Cert, c1Key, err := c1KG.Generate(ServerClientCA, nil, pkix.Name{CommonName: "TestName1"}, time.Now(), 1*time.Hour)
	if err != nil {
		t.Fatal(err)
	}

	// generate c2 cert and key
	c2KG := NewKeygenECDSA(EcdsaCurveP521)
	err = c2KG.SetCaCert(c1Cert, c1Key)
	if err != nil {
		t.Fatal(err)
	}
	c2Cert, c2Key, err := c2KG.Generate(ServerClientCA, nil, pkix.Name{CommonName: "TestName1"}, time.Now(), 1*time.Hour)
	if err != nil {
		t.Fatal(err)
	}

	// generate c3 cert and key
	c3KG := NewKeygenECDSA(EcdsaCurveP521)
	err = c3KG.SetCaCert(c2Cert, c2Key)
	if err != nil {
		t.Fatal(err)
	}
	c3Cert, c3Key, err := c3KG.Generate(ServerClient, nil, pkix.Name{CommonName: "TestName1"}, time.Now(), 1*time.Hour)
	if err != nil {
		t.Fatal(err)
	}

	// generate c4 cert and key
	// this is a cert created from c3 - a certificate without ca flag and without x509.KeyUsageCertSign
	c4KG := NewKeygenECDSA(EcdsaCurveP521)
	err = c4KG.SetCaCert(c3Cert, c3Key)
	if err != nil {
		t.Fatal(err)
	}
	c4Cert, _, err := c4KG.Generate(ServerClient, nil, pkix.Name{CommonName: "TestName1"}, time.Now(), 1*time.Hour)
	if err != nil {
		t.Fatal(err)
	}

	// valid chain for c3 from ca over c1 over c2
	err = VerifyCertificate(c3Cert, caCert, c1Cert, c2Cert)
	if err != nil {
		t.Fatal(err)
	}

	// invalid chain for c3 from ca over c2 missing c1
	err = VerifyCertificate(c3Cert, caCert, c2Cert)
	if err == nil {
		t.Fatal("expected error")
	}

	// valid chain for c3 from c1 over c2
	err = VerifyCertificate(c3Cert, c1Cert, c2Cert)
	if err != nil {
		t.Fatal(err)
	}

	// invalid chain for c3 from c1 missing c2
	err = VerifyCertificate(c3Cert, c1Cert)
	if err == nil {
		t.Fatal("expected error")
	}

	// valid chain for c3 from c2
	err = VerifyCertificate(c3Cert, c2Cert)
	if err != nil {
		t.Fatal(err)
	}

	// invalid chain for c3 from c1
	err = VerifyCertificate(c3Cert, c1Cert)
	if err == nil {
		t.Fatal("expected error")
	}

	// valid chain for c2 from ca over c1
	err = VerifyCertificate(c2Cert, caCert, c1Cert)
	if err != nil {
		t.Fatal(err)
	}

	// invalid chain for c2 from ca missing c1
	err = VerifyCertificate(c2Cert, caCert)
	if err == nil {
		t.Fatal("expected error")
	}

	// valid chain for c2 from c1
	err = VerifyCertificate(c2Cert, c1Cert)
	if err != nil {
		t.Fatal(err)
	}

	// valid chain for c1 from ca
	err = VerifyCertificate(c1Cert, caCert)
	if err != nil {
		t.Fatal(err)
	}

	// invalid chain because it was from a certificate without ca flag and without x509.KeyUsageCertSign
	//
	// this ensures Keygen.Generate(false) is working as expected and we are not accidentally generating
	// certificates able to create other certificates
	err = VerifyCertificate(c4Cert, c3Cert)
	if err == nil {
		t.Fatal("expected error")
	}
	if !strings.Contains(err.Error(), "parent certificate cannot sign this kind of certificate") {
		t.Fatal("unexpected error")
	}
}

func TestKeygenECDSABasic(t *testing.T) {

	// generate ca and keys
	caKG := NewKeygenECDSA(EcdsaCurveP521)
	caCert, caKey, err := caKG.Generate(ServerClientCA, nil, pkix.Name{CommonName: "TestName1"}, time.Now(), 1*time.Hour)
	if err != nil {
		t.Fatal(err)
	}

	// generate client ca and keys
	clientKG := NewKeygenECDSA(EcdsaCurveP521)
	err = clientKG.SetCaCert(caCert, caKey)
	if err != nil {
		t.Fatal(err)
	}
	clientCert, _, err := clientKG.Generate(ServerClient, nil, pkix.Name{CommonName: "TestName2"}, time.Now(), 1*time.Hour)
	if err != nil {
		t.Fatal(err)
	}

	// verify valid certificate
	err = VerifyCertificate(clientCert, caCert)
	if err != nil {
		t.Fatal(err)
	}

	// generate a new certificate and try to Verify
	badKG := NewKeygenECDSA(EcdsaCurveP521)
	badCert, _, err := badKG.Generate(ServerClient, nil, pkix.Name{CommonName: "TestName3"}, time.Now(), 1*time.Hour)
	if err != nil {
		t.Fatal(err)
	}
	err = VerifyCertificate(badCert, caCert)
	if err == nil {
		t.Fatal("expected error")
	}
}
