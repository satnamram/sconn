package sconn

import (
	"crypto/rand"
	"crypto/tls"
	"crypto/x509"
	"encoding/pem"
	"errors"
	"fmt"
	"net"
	"sync"
	"time"
)

// CA holds the
type Config struct {
	rawCaCerts [][]byte
	rawMyCerts [][]byte
	rawMyKeys  [][]byte
	caCerts    *x509.CertPool
	myCerts    []tls.Certificate
}

func NewConfig() *Config {
	return &Config{
		rawCaCerts: [][]byte{},
		rawMyCerts: [][]byte{},
		rawMyKeys:  [][]byte{},
		caCerts:    x509.NewCertPool(),
		myCerts:    []tls.Certificate{},
	}
}

// Trust a CA Certificate.
func (cfg *Config) Trust(certPEMBlock []byte) *Config {
	cfg.rawCaCerts = append(cfg.rawCaCerts, certPEMBlock)
	return cfg
}

// Present a Certificate.
func (cfg *Config) Present(certPEMBlock []byte, keyPEMBlock []byte) *Config {
	cfg.rawMyCerts = append(cfg.rawMyCerts, certPEMBlock)
	cfg.rawMyKeys = append(cfg.rawMyKeys, keyPEMBlock)
	return cfg
}

// Build configuration.
func (cfg *Config) Build() (*Config, error) {
	for _, certPEMBlock := range cfg.rawCaCerts {
		block, _ := pem.Decode(certPEMBlock)
		if block == nil || block.Type != "CERTIFICATE" {
			return nil, errors.New("failed to decode PEM block containing ca certificate")
		}
		cert, err := x509.ParseCertificate(block.Bytes)
		if err != nil {
			return nil, err
		}
		cfg.caCerts.AddCert(cert)
	}
	for i := range cfg.rawMyCerts {
		cert, err := tls.X509KeyPair(cfg.rawMyCerts[i], cfg.rawMyKeys[i])
		if err != nil {
			return nil, err
		}
		cfg.myCerts = append(cfg.myCerts, cert)
	}

	// clear raw certificates
	cfg.rawCaCerts = nil
	cfg.rawMyCerts = nil
	return cfg, nil
}

type Listener struct {
	wg       sync.WaitGroup
	mutex    sync.Mutex
	closed   bool
	listener net.Listener
}

func (l *Listener) Addr() net.Addr {
	return l.listener.Addr()
}

func (l *Listener) Close() error {
	l.mutex.Lock()
	l.closed = true
	l.mutex.Unlock()
	err := l.listener.Close()
	l.wg.Wait()
	return err

}

func (cfg *Config) Listen(addr string, handler func(conn *tls.Conn)) (*Listener, error) {

	// create tls config with forced client auth
	config := tls.Config{
		Certificates: cfg.myCerts,
		ClientAuth:   tls.RequireAndVerifyClientCert,
		ClientCAs:    cfg.caCerts,
	}
	config.Rand = rand.Reader

	// create the listener
	l, err := tls.Listen("tcp", addr, &config)
	if err != nil {
		return nil, err
	}

	// create dispatcher routine
	listener := &Listener{listener: l}
	listener.wg.Add(1)
	go func() {
		defer listener.wg.Done()
		for {

			// accept new connection
			conn, err := listener.listener.Accept()
			if err != nil {
				listener.mutex.Lock()
				closed := listener.closed
				listener.mutex.Unlock()
				if closed {
					return // DONE
				}
				fmt.Println(err.Error())
				continue
			}

			// get tls connection, force handshake, validate
			tlsConn := conn.(*tls.Conn)
			tlsConn.Handshake()
			state := tlsConn.ConnectionState()
			if state.HandshakeComplete == false {
				fmt.Println("tls handshake failed")
				continue
			}

			// dispatch to new routine!
			go handler(tlsConn)
		}
	}()

	return listener, nil
}

func (cfg *Config) Dial(addr string) (*tls.Conn, error) {

	// create tls config for the server
	config := tls.Config{
		Certificates: cfg.myCerts,
		RootCAs:      cfg.caCerts,
	}
	config.Rand = rand.Reader

	// establish connection
	tlsConn, err := tls.Dial("tcp", addr, &config)
	if err != nil {
		return nil, err
	}

	// get tls connection, force handshake, validate
	tlsConn.Handshake()
	state := tlsConn.ConnectionState()
	if state.HandshakeComplete == false {
		return nil, errors.New("tls handshake failed")
	}

	return tlsConn, nil
}

type Tunnel struct {
	cfg     *Config
	addr    string
	handler func(conn *tls.Conn)

	wg     sync.WaitGroup
	mutex  sync.Mutex
	conn   *tls.Conn
	closed bool
}

func (cfg *Config) Tunnel(addr string, handler func(conn *tls.Conn)) (*Tunnel, error) {
	tun := &Tunnel{
		addr:    addr,
		handler: handler,
		cfg:     cfg,
	}
	tun.wg.Add(1)
	go func() {
		defer tun.wg.Done()
		for {

			// if tunnel is closed exit here
			tun.mutex.Lock()
			if tun.closed {
				tun.mutex.Unlock()
				return
			}
			tun.mutex.Unlock()

			// dial
			conn, err := cfg.Dial(tun.addr)
			if err != nil {
				println(err.Error())
				time.Sleep(5 * time.Second)
				continue
			}

			// save current connection for closing
			tun.mutex.Lock()
			tun.conn = conn
			tun.mutex.Unlock()

			// run in handler until reconnect
			tun.handler(conn)
		}
	}()

	return tun, nil
}

func (tun *Tunnel) Close() error {
	tun.mutex.Lock()
	tun.closed = true
	err := tun.conn.Close()
	tun.mutex.Unlock()
	tun.wg.Wait()
	return err
}
