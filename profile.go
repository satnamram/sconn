package sconn

import (
	"bytes"
	"errors"
	"regexp"
)

type Profile struct {
	Host   string
	Blocks [][]byte
}

func ParseProfile(b []byte) (*Profile, error) {
	in := string(b)
	p := &Profile{}

	exp, err := regexp.Compile(`(?im)HOST\s*(\S*).*?$`)
	if err != nil {
		return nil, err
	}

	submatch := exp.FindAllStringSubmatch(in, -1)
	if len(submatch) != 1 && len(submatch[0]) != 2 {
		return nil, errors.New("no host entry in sconn profile")
	}
	p.Host = submatch[0][1]

	exp, err = regexp.Compile(`(?ims)-----BEGIN.*?.-----END.*?-----\n`)
	if err != nil {
		return nil, err
	}
	matches := exp.FindAllString(in, -1)
	for _, match := range matches {
		p.Blocks = append(p.Blocks, []byte(match))
	}
	return p, nil
}

func (p Profile) Encode() []byte {
	var buf bytes.Buffer
	buf.Write([]byte("HOST " + p.Host))
	buf.Write([]byte("\n\n"))
	for i, block := range p.Blocks {
		if i > 0 {
			buf.Write([]byte("\n"))
		}
		buf.Write(block)
	}
	return buf.Bytes()
}
