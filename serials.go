package sconn

import (
	"bytes"
	"crypto/rand"
	"errors"
	"io/ioutil"
	"math/big"
	"os"
	"sync"
)

type serialsStore struct {
	mutex    sync.Mutex
	filename string
	serials  [][16]byte
	checksum [16]byte
}

func loadSerials(filename string) (*serialsStore, error) {

	// if file not exist return empty store
	_, err := os.Stat(filename)
	if err != nil {
		return &serialsStore{
			filename: filename,
			serials:  [][16]byte{},
		}, nil
	}

	// read file
	b, err := ioutil.ReadFile(filename)
	if err != nil {
		return nil, err
	}

	// return empty store in case the file contains no bytes
	store := &serialsStore{
		filename: filename,
		serials:  [][16]byte{},
	}
	if len(b) == 0 {
		return store, nil
	}

	// validate input
	if len(b)%16 != 0 || len(b) < 32 {
		return nil, errors.New("serials store corrupted - bad size")
	}

	// copy over checksum and remove first 16 bytes
	copy(store.checksum[:], b[:16])
	b = b[16:]

	// copy over serials
	for i := 0; i < len(b); i = i + 16 {
		var serial [16]byte
		copy(serial[:], b[i:i+16])
		store.serials = append(store.serials, serial)
	}

	return store, nil
}

func (ss *serialsStore) generate() (*big.Int, error) {

	// !!! handle serialsStore == nil
	if ss == nil {
		serialNumberLimit := new(big.Int).Lsh(big.NewInt(1), 128)
		return rand.Int(rand.Reader, serialNumberLimit)
	}

	ss.mutex.Lock()
	defer ss.mutex.Unlock()

	var uniqueSerial *big.Int
	for {
		serialNumberLimit := new(big.Int).Lsh(big.NewInt(1), 128)
		serialNumber, err := rand.Int(rand.Reader, serialNumberLimit)
		if err != nil {
			return nil, err
		}
		serialNumberBytes := serialNumber.Bytes()

		// ensure length
		if len(serialNumberBytes) != 16 {
			continue
		}

		// ensure serial is unique
		if ss.contains(serialNumberBytes) {
			continue
		}

		uniqueSerial = serialNumber
		break
	}
	return uniqueSerial, nil
}

func (ss *serialsStore) add(serial []byte) error {

	// !!! handle serialsStore == nil
	if ss == nil {
		return nil
	}

	ss.mutex.Lock()
	defer ss.mutex.Unlock()

	if len(serial) != 16 {
		return errors.New("bad serial size (!= 16)")
	}

	// check store does not contain the serial already
	if ss.contains(serial) {
		return errors.New("serial already exists")
	}

	// add serial
	var newSerial [16]byte
	copy(newSerial[:], serial)
	ss.serials = append(ss.serials, newSerial)

	// update checksum
	for i := 0; i < 16; i++ {
		ss.checksum[i] += serial[i]
	}

	// Save!
	var buf bytes.Buffer

	// prepend checksum
	_, err := buf.Write(ss.checksum[:])
	if err != nil {
		return err
	}

	// add all serials
	for _, serial := range ss.serials {
		_, err := buf.Write(serial[:])
		if err != nil {
			return err
		}
	}

	// write file to disk
	return ioutil.WriteFile(ss.filename, buf.Bytes(), os.ModePerm)
}

// not thread safe - called by add and generate
func (ss *serialsStore) contains(serial []byte) bool {
	if len(serial) != 16 {
		return false
	}
	for _, existingSerial := range ss.serials {
		if bytes.Equal(existingSerial[:], serial) {
			return true
		}
	}
	return false
}
