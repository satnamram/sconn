package sconn

import (
	"bytes"
	"os"
	"testing"
)

func TestFiles(t *testing.T) {
	b1 := []byte("hello world")
	b2 := []byte("bazinga")
	b3 := []byte("wabbajack")

	defer os.Remove("b1")
	defer os.Remove("b2")
	defer os.Remove("b3")
	ret, err := Files("./", []string{"b1", "b2", "b3"}, func() ([][]byte, error) {
		return [][]byte{b1, b2, b3}, nil
	})
	if err != nil {
		t.Fatal(err)
	}
	if !bytes.Equal(b1, ret[0]) {
		t.Fatal("wrong bytes")
	}
	if !bytes.Equal(b2, ret[1]) {
		t.Fatal("wrong bytes")
	}
	if !bytes.Equal(b3, ret[2]) {
		t.Fatal("wrong bytes")
	}

	ret, err = Files("./", []string{"b1", "b2", "b3"}, func() ([][]byte, error) {
		return [][]byte{[]byte("a"), []byte("b"), []byte("c")}, nil
	})
	if err != nil {
		t.Fatal(err)
	}
	if !bytes.Equal(b1, ret[0]) {
		t.Fatal("wrong bytes")
	}
	if !bytes.Equal(b2, ret[1]) {
		t.Fatal("wrong bytes")
	}
	if !bytes.Equal(b3, ret[2]) {
		t.Fatal("wrong bytes")
	}
}
