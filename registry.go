package sconn

import (
	"bytes"
	"crypto/rand"
	"crypto/x509"
	"encoding/json"
	"encoding/pem"
	"errors"
	"io/ioutil"
	"math/big"
	"os"
	"path/filepath"
	"strconv"
	"sync"
)

func LoadRegistry(dir string, organization string, uniqueCN bool) (*Registry, error) {

	// ensure directory exist
	if dir != "" {
		err := os.MkdirAll(dir, os.ModePerm)
		if err != nil {
			return nil, err
		}
	}

	// check if organization.json exist
	registryPath := filepath.Join(dir, organization) + ".json"
	registryExist := false
	_, err := os.Stat(registryPath)
	if err == nil {
		registryExist = true
	}

	// create/load registry file
	var registry *Registry
	if registryExist {

		// read registry from disk
		registryBytes, err := ioutil.ReadFile(registryPath)
		if err != nil {
			return nil, err
		}

		registry = &Registry{}
		err = json.Unmarshal(registryBytes, registry)
		if err != nil {
			return nil, err
		}
		registry.dir = dir
		registry.path = registryPath
	} else {

		// write empty registry to disk
		registry = &Registry{
			dir:      dir,
			path:     registryPath,
			UniqueCN: uniqueCN,
			Counter:  0,
			Certs:    []*RegistryEntry{},
		}

		err = registry.save()
		if err != nil {
			return nil, err
		}
	}

	return registry, nil
}

func (reg *Registry) Add(certPEMBlock []byte) error {
	reg.mutex.Lock()
	defer reg.mutex.Unlock()

	// decode certificate
	block, _ := pem.Decode(certPEMBlock)
	if block == nil || block.Type != "CERTIFICATE" {
		return errors.New("failed to decode PEM block containing ca certificate")
	}
	cert, err := x509.ParseCertificate(block.Bytes)
	if err != nil {
		return err
	}

	// check if serial is unique
	for _, entry := range reg.Certs {
		if bytes.Equal(entry.Serial, cert.SerialNumber.Bytes()) {
			return errors.New("serial does already exist")
		}
	}

	// (optional) check if common name is unique
	if reg.UniqueCN {
		for _, entry := range reg.Certs {
			if entry.Name == cert.Subject.CommonName {
				return errors.New("name \"" + entry.Name + "\" does already exist")
			}
		}
	}

	// add to index
	reg.Counter++
	entry := &RegistryEntry{
		ID:     reg.Counter,
		Name:   cert.Subject.CommonName,
		Serial: cert.SerialNumber.Bytes(),
	}
	reg.Certs = append(reg.Certs, entry)

	// save it
	err = reg.save()
	if err != nil {
		return err
	}
	err = ioutil.WriteFile(filepath.Join(reg.dir, entry.FileName()), certPEMBlock, 0600)
	if err != nil {
		return err
	}

	return nil
}

type Registry struct {
	mutex    sync.Mutex
	dir      string
	path     string
	UniqueCN bool             `json:"unique"`
	Counter  int              `json:"counter"`
	Certs    []*RegistryEntry `json:"certs"`
}

func (reg *Registry) Generate() (*big.Int, error) {
	var uniqueSerial *big.Int
	for {
		serialNumberLimit := new(big.Int).Lsh(big.NewInt(1), 128)
		serialNumber, err := rand.Int(rand.Reader, serialNumberLimit)
		if err != nil {
			return nil, err
		}
		serialNumberBytes := serialNumber.Bytes()

		// ensure length
		if len(serialNumberBytes) != 16 {
			continue
		}

		// ensure serial is unique
		for _, entry := range reg.Certs {
			if bytes.Equal(entry.Serial, serialNumberBytes) {
				continue
			}
		}

		uniqueSerial = serialNumber
		break
	}
	return uniqueSerial, nil
}

func (reg *Registry) Revoke(serial []byte) error {
	for _, entry := range reg.Certs {
		if bytes.Equal(entry.Serial, serial) {
			entry.Revoked = true
			return reg.save()
		}
	}
	return errors.New("serial not found in registry")
}

// GetEntries of registry.
func (reg *Registry) Entries() []*RegistryEntry {
	return reg.Certs
}

// get list of all certificates
func (reg *Registry) Certificates() ([]*x509.Certificate, error) {
	certs := make([]*x509.Certificate, len(reg.Certs))
	for i, entry := range reg.Certs {
		certPEMBlock, err := ioutil.ReadFile(filepath.Join(reg.dir, entry.FileName()))
		if err != nil {
			return nil, err
		}
		block, _ := pem.Decode(certPEMBlock)
		if block == nil || block.Type != "CERTIFICATE" {
			return nil, errors.New("failed to decode PEM block containing ca certificate")
		}
		cert, err := x509.ParseCertificate(block.Bytes)
		if err != nil {
			return nil, err
		}
		certs[i] = cert
	}
	return certs, nil
}

// get a pool with all loadable certificates
func (reg *Registry) CertificatePool() (*x509.CertPool, error) {
	pool := x509.NewCertPool()
	certs, err := reg.Certificates()
	if err != nil {
		return nil, err
	}
	for _, cert := range certs {
		pool.AddCert(cert)
	}
	return pool, nil
}

type RegistryEntry struct {
	ID      int    `json:"id"`
	Name    string `json:"name"`
	Serial  []byte `json:"serial"`
	Revoked bool   `json:"revoked,omitempty"`
}

func (entry *RegistryEntry) FileName() string {
	fileName := strconv.Itoa(entry.ID)
	if len(entry.Name) > 0 {
		fileName = fileName + "_" + entry.Name
	}
	fileName = fileName + ".cert"
	return fileName
}

func (reg *Registry) save() error {
	registryBytes, err := json.MarshalIndent(reg, "", "  ")
	if err != nil {
		return err
	}
	err = ioutil.WriteFile(reg.path, registryBytes, 0600)
	if err != nil {
		return err
	}
	return nil
}
