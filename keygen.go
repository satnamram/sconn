package sconn

import (
	"bytes"
	"crypto"
	"crypto/ecdsa"
	"crypto/elliptic"
	"crypto/rand"
	"crypto/rsa"
	"crypto/tls"
	"crypto/x509"
	"crypto/x509/pkix"
	"encoding/pem"
	"errors"
	"fmt"
	"math/big"
	"net"
	"os"
	"time"
)

type KeyType int

const (
	Server         KeyType = 0
	Client         KeyType = 1
	ServerClient   KeyType = 2
	ServerCA       KeyType = 3
	ClientCA       KeyType = 4
	ServerClientCA KeyType = 5
)

type EcdsaCurve string

const (
	EcdsaCurveP224 EcdsaCurve = "P224"
	EcdsaCurveP256 EcdsaCurve = "P256"
	EcdsaCurveP384 EcdsaCurve = "P384"
	EcdsaCurveP521 EcdsaCurve = "P521"
)

type Keygen struct {
	caCert     *x509.Certificate
	caKey      crypto.PrivateKey
	rsaBits    int
	ecdsaCurve EcdsaCurve
}

func NewKeygenRSA(rsaBits int) *Keygen {
	return &Keygen{
		rsaBits: rsaBits,
	}
}

func NewKeygenECDSA(ecdsaCurve EcdsaCurve) *Keygen {
	return &Keygen{
		ecdsaCurve: ecdsaCurve,
	}
}

func (kg *Keygen) SetCaCert(certPEMBlock []byte, keyPEMBlock []byte) error {
	catls, err := tls.X509KeyPair(certPEMBlock, keyPEMBlock)
	if err != nil {
		return err
	}
	ca, err := x509.ParseCertificate(catls.Certificate[0])
	if err != nil {
		return err
	}
	kg.caCert = ca
	kg.caKey = catls.PrivateKey
	return nil
}

func VerifyCertificate(cert []byte, ca []byte, i ...[]byte) error {
	block, _ := pem.Decode([]byte(cert))
	if block == nil {
		return errors.New("failed to parse certificate PEM")
	}
	c, err := x509.ParseCertificate(block.Bytes)
	if err != nil {
		return errors.New("failed to parse certificate: " + err.Error())
	}
	roots := x509.NewCertPool()
	intermediates := x509.NewCertPool()
	ok := roots.AppendCertsFromPEM([]byte(ca))
	if !ok {
		return errors.New("failed to parse root certificate")
	}
	for _, intermediate := range i {
		ok := intermediates.AppendCertsFromPEM([]byte(intermediate))
		if !ok {
			return errors.New("failed to parse root certificate")
		}

	}
	opts := x509.VerifyOptions{
		Roots:         roots,
		Intermediates: intermediates,
	}
	if _, err := c.Verify(opts); err != nil {
		return errors.New("failed to verify certificate: " + err.Error())
	}
	return nil
}

func (kg *Keygen) Generate(t KeyType, serial *big.Int, subject pkix.Name, validFrom time.Time, validFor time.Duration) ([]byte, []byte, error) {

	// generate random serial in case it is missing
	var err error
	if serial == nil {
		serialNumberLimit := new(big.Int).Lsh(big.NewInt(1), 128)
		serial, err = rand.Int(rand.Reader, serialNumberLimit)
		if err != nil {
			return nil, nil, err
		}
	}

	// generate keypair
	var priv interface{}
	switch kg.ecdsaCurve {
	case "":
		priv, err = rsa.GenerateKey(rand.Reader, kg.rsaBits)
	case "P224":
		priv, err = ecdsa.GenerateKey(elliptic.P224(), rand.Reader)
	case "P256":
		priv, err = ecdsa.GenerateKey(elliptic.P256(), rand.Reader)
	case "P384":
		priv, err = ecdsa.GenerateKey(elliptic.P384(), rand.Reader)
	case "P521":
		priv, err = ecdsa.GenerateKey(elliptic.P521(), rand.Reader)
	default:
		return nil, nil, errors.New("Unrecognized elliptic curve: " + string(kg.ecdsaCurve))
	}
	if err != nil {
		return nil, nil, err
	}

	// set key usage by type
	extKeyUsage := []x509.ExtKeyUsage{}
	if t == Client || t == ServerClient || t == ClientCA || t == ServerClientCA {
		extKeyUsage = append(extKeyUsage, x509.ExtKeyUsageClientAuth)
	}
	if t == Server || t == ServerClient || t == ServerCA || t == ServerClientCA {
		extKeyUsage = append(extKeyUsage, x509.ExtKeyUsageServerAuth)
	}

	// create x509 template
	template := x509.Certificate{
		SerialNumber:          serial,
		Subject:               subject,
		NotBefore:             validFrom,
		NotAfter:              validFrom.Add(validFor),
		KeyUsage:              x509.KeyUsageKeyEncipherment | x509.KeyUsageDigitalSignature,
		ExtKeyUsage:           extKeyUsage,
		BasicConstraintsValid: true,
	}
	template.IPAddresses = append(template.IPAddresses, net.ParseIP("127.0.0.1"))

	// enable this certificate to be used as a certification authority
	if t == ClientCA || t == ServerCA || t == ServerClientCA {
		template.KeyUsage |= x509.KeyUsageCertSign
		template.IsCA = true
	}

	// create certificate (optionally sign it)
	var derBytes []byte
	if kg.caCert == nil {
		derBytes, err = x509.CreateCertificate(rand.Reader, &template, &template, publicKey(priv), priv)
		if err != nil {
			return nil, nil, err
		}
	} else {
		derBytes, err = x509.CreateCertificate(rand.Reader, &template, kg.caCert, publicKey(priv), kg.caKey)
		if err != nil {
			return nil, nil, err
		}
	}

	// encode
	var certBuf bytes.Buffer
	pem.Encode(&certBuf, &pem.Block{Type: "CERTIFICATE", Bytes: derBytes})
	certBytes := certBuf.Bytes()
	var keyBuf bytes.Buffer
	pem.Encode(&keyBuf, pemBlockForKey(priv))
	keyBytes := keyBuf.Bytes()

	return certBytes, keyBytes, nil
}

func publicKey(priv interface{}) interface{} {
	switch k := priv.(type) {
	case *rsa.PrivateKey:
		return &k.PublicKey
	case *ecdsa.PrivateKey:
		return &k.PublicKey
	default:
		return nil
	}
}

func pemBlockForKey(priv interface{}) *pem.Block {
	switch k := priv.(type) {
	case *rsa.PrivateKey:
		return &pem.Block{Type: "RSA PRIVATE KEY", Bytes: x509.MarshalPKCS1PrivateKey(k)}
	case *ecdsa.PrivateKey:
		b, err := x509.MarshalECPrivateKey(k)
		if err != nil {
			fmt.Fprintf(os.Stderr, "Unable to marshal ECDSA private key: %v", err)
			os.Exit(2)
		}
		return &pem.Block{Type: "EC PRIVATE KEY", Bytes: b}
	default:
		return nil
	}
}
