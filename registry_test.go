package sconn

import (
	"bytes"
	"crypto/x509"
	"crypto/x509/pkix"
	"encoding/pem"
	"os"
	"testing"
	"time"
)

func TestRegistry(t *testing.T) {
	defer os.Remove("testOrganization.json")
	defer os.Remove("1_testName.cert")

	// initially create
	reg, err := LoadRegistry("", "testOrganization", true)
	if err != nil {
		t.Fatal(err)
	}

	// reload and check for unique flag
	regX, err := LoadRegistry("", "testOrganization", false)
	if err != nil {
		t.Fatal(err)
	}
	if !registryEquals(reg, regX) {
		t.Fatal("reg and regX not equal")
	}

	if regX.UniqueCN == false {
		t.Fatal("unique flag not loaded")
	}

	// generate serial and generate a certificate with it
	serial, err := reg.Generate()
	if err != nil {
		t.Fatal(err)
	}
	cert, _, err := NewKeygenECDSA(EcdsaCurveP521).
		Generate(ServerClientCA, serial, pkix.Name{CommonName: "testName"}, time.Now(), 1*time.Hour)
	if err != nil {
		t.Fatal(err)
	}

	block, _ := pem.Decode(cert)
	if block == nil || block.Type != "CERTIFICATE" {
		t.Fatal("decode error")
	}
	certx509, err := x509.ParseCertificate(block.Bytes)
	if err != nil {
		t.Fatal(err)
	}

	// add to registry
	err = reg.Add(cert)
	if err != nil {
		t.Fatal(err)
	}

	// check if certificate equals the generated one
	certsx509, err := reg.Certificates()
	if err != nil {
		t.Fatal(err)
	}
	if !bytes.Equal(certsx509[0].Raw, certx509.Raw) {
		t.Fatal("certs does not equal")
	}

	// generate serial with duplicate serial and create error
	certDuplicateSerial, _, err := NewKeygenECDSA(EcdsaCurveP521).
		Generate(ServerClientCA, serial, pkix.Name{CommonName: "testName"}, time.Now(), 1*time.Hour)
	if err != nil {
		t.Fatal(err)
	}
	err = reg.Add(certDuplicateSerial)
	if err.Error() != "serial does already exist" {
		t.Fatal("expected error")
	}

	// generate serial with duplicate common name and create error
	serial2, err := reg.Generate()
	if err != nil {
		t.Fatal(err)
	}
	certDuplicateCN, _, err := NewKeygenECDSA(EcdsaCurveP521).
		Generate(ServerClientCA, serial2, pkix.Name{CommonName: "testName"}, time.Now(), 1*time.Hour)
	if err != nil {
		t.Fatal(err)
	}
	err = reg.Add(certDuplicateCN)
	if err.Error() != `name "testName" does already exist` {
		t.Fatal("expected error")
	}

	// reload and check if entry equals
	regXX, err := LoadRegistry("", "testOrganization", false)
	if err != nil {
		t.Fatal(err)
	}
	if !registryEquals(reg, regXX) {
		t.Fatal("reg and regXX not equal")
	}

	// revoke the certificate
	err = reg.Revoke(serial.Bytes())
	if err != nil {
		t.Fatal(err)
	}

	// reload and check if certificate is present
	regXXX, err := LoadRegistry("", "testOrganization", false)
	if err != nil {
		t.Fatal(err)
	}
	if !registryEquals(reg, regXXX) {
		t.Fatal("reg and regXXX not equal")
	}

	// validate outcome in fs
	mustExist(t, "testOrganization.json")
	mustExist(t, "1_testName.cert")
}

func mustExist(t *testing.T, path string) bool {
	info, err := os.Stat(path)
	if err != nil {
		t.Fatal("file " + path + " does not exist")
	}
	if info.IsDir() {
		t.Fatal("file " + path + " is directory")
	}
	return true
}

func registryEquals(r1 *Registry, r2 *Registry) bool {
	if r1.dir != r2.dir {
		return false
	}
	if r1.path != r2.path {
		return false
	}
	if r1.Counter != r2.Counter {
		return false
	}
	if r1.UniqueCN != r2.UniqueCN {
		return false
	}
	if len(r1.Certs) != len(r2.Certs) {
		return false
	}
	for i := range r1.Certs {
		if !entryEquals(r1.Certs[i], r2.Certs[i]) {
			return false
		}
	}
	return true
}

func entryEquals(e1 *RegistryEntry, e2 *RegistryEntry) bool {
	if e1.ID != e2.ID {
		return false
	}
	if e1.Name != e2.Name {
		return false
	}
	if e1.Revoked != e2.Revoked {
		return false
	}
	if !bytes.Equal(e1.Serial, e2.Serial) {
		return false
	}
	return true
}
