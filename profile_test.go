package sconn

import (
	"bytes"
	"crypto/x509/pkix"
	"strconv"
	"testing"
	"time"
)

func TestProfile(t *testing.T) {
	caKG := NewKeygenECDSA(EcdsaCurveP521)
	caCert, caKey, err := caKG.Generate(ServerClientCA, nil, pkix.Name{CommonName: "TestName1"}, time.Now(), 1*time.Hour)
	if err != nil {
		t.Fatal(err)
	}
	p := &Profile{
		Host:   "myhost",
		Blocks: [][]byte{caCert, caKey},
	}
	pp, err := ParseProfile(p.Encode())
	if err != nil {
		t.Fatal(err)
	}
	if p.Host != pp.Host {
		t.Fatal("expected different host")
	}
	if len(p.Blocks) != len(pp.Blocks) {
		t.Fatal("len(Blocks) != 2")
	}
	for i := range pp.Blocks {
		if !bytes.Equal(pp.Blocks[i], p.Blocks[i]) {
			t.Fatal("block " + strconv.Itoa(i) + " does not equal")
		}
	}
}
