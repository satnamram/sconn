package sconn

import (
	"bytes"
	"os"
	"testing"
)

var (
	zeroSerial  = []byte{0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0}
	testSerial1 = []byte{0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1}
	testSerial2 = []byte{31, 0, 0, 0, 0, 0, 2, 0, 0, 0, 0, 0, 0, 0, 0, 1}
	testSerial3 = []byte{0, 0, 0, 0, 0, 222, 0, 0, 0, 123, 0, 0, 0, 0, 0, 1}
)

func TestSerial(t *testing.T) {
	defer os.Remove("serials.store")
	ss, err := loadSerials("serials.store")
	if err != nil {
		t.Fatal(err)
	}
	err = ss.add(testSerial1)
	if err != nil {
		t.Fatal(err)
	}
	if !ss.contains(testSerial1) {
		t.Fatal("must contain")
	}
	err = ss.add(testSerial2)
	if err != nil {
		t.Fatal(err)
	}

	// generate
	generatedSerial, err := ss.generate()
	if err != nil {
		t.Fatal(err)
	}
	err = ss.add(generatedSerial.Bytes())
	if err != nil {
		t.Fatal(err)
	}
	if bytes.Equal(zeroSerial, generatedSerial.Bytes()) {
		t.Fatal("generated serial is zero")
	}

	// reload!
	ss, err = loadSerials("serials.store")
	if err != nil {
		t.Fatal(err)
	}
	if !ss.contains(testSerial1) {
		t.Fatal("must contain")
	}
	if !ss.contains(testSerial2) {
		t.Fatal("must contain")
	}
	if !ss.contains(generatedSerial.Bytes()) {
		t.Fatal("must contain")
	}
	if ss.contains(testSerial3) {
		t.Fatal("must not contain")
	}
}
