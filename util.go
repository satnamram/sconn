package sconn

import (
	"errors"
	"io/ioutil"
	"os"
	"path/filepath"
	"crypto/tls"
	"crypto/sha1"
	"encoding/hex"
)

// TODO: keep in mind we only hash verified chains here - test it if using more than one segment!
func Hash(conn *tls.Conn) string{
	state := conn.ConnectionState()
	if len(state.VerifiedChains) == 0 {
		return ""
	}
	hasher := sha1.New()
	for _, chain := range state.VerifiedChains {
		for _, cert := range chain {
			hasher.Write(cert.Raw)
		}
	}
	return hex.EncodeToString(hasher.Sum(nil))
}

func Files(dir string, files []string, f func() ([][]byte, error)) ([][]byte, error) {

	// check if all files exist - if not call function and save them
	for _, file := range files {
		info, err := os.Stat(filepath.Join(dir, file))
		if err != nil {

			// create the content
			content, err := f()
			if err != nil {
				return nil, err
			}
			if content == nil {
				return nil, errors.New("nil return value")
			}
			if len(content) != len(files) {
				return nil, errors.New("wrong number of byte arrays")
			}

			// write to disk
			for i, file := range files {
				if content[i] != nil {
					err := ioutil.WriteFile(filepath.Join(dir, file), content[i], 0600)
					if err != nil {
						return nil, err
					}
				}
			}

			return content, nil
		}
		if info.IsDir() {
			return nil, errors.New(info.Name() + " is a directory")
		}
		// file exist
	}

	// read all the content
	content := [][]byte{}
	for _, file := range files {
		fileName := filepath.Join(dir, file)
		b, err := ioutil.ReadFile(fileName)
		if err != nil {
			return nil, errors.New("error reading " + fileName)
		}
		content = append(content, b)
	}
	return content, nil
}
